import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CategoriesListComponent } from './categories/components/categories-list/categories-list.component';
import { CoursesListComponent } from './courses/components/courses-list/courses-list.component';
import { CategoryEditDialogComponent } from './categories/components/category-edit-dialog/category-edit-dialog.component';
import { CategoryDeleteDialogComponent } from './categories/components/category-delete-dialog/category-delete-dialog.component';
import { CourseEditComponent } from './courses/components/course-edit/course-edit.component';
import { CourseDeleteDialogComponent } from './courses/components/course-delete-dialog/course-delete-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesListComponent,
    CoursesListComponent,
    CategoryEditDialogComponent,
    CategoryDeleteDialogComponent,
    CourseEditComponent,
    CourseDeleteDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
