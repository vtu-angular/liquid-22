import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CategoriesListComponent } from './categories/components/categories-list/categories-list.component';
import { CourseEditComponent } from './courses/components/course-edit/course-edit.component';
import { CoursesListComponent } from './courses/components/courses-list/courses-list.component';

const routes: Routes = [
  {
    path: 'categories',
    component: CategoriesListComponent
  },
  {
    path: 'courses',
    component: CoursesListComponent
  },
  {
    path: 'courses/create',
    component: CourseEditComponent
  },
  {
    path: 'courses/edit/:id',
    component: CourseEditComponent
  },
  {
    path: '',
    redirectTo: 'courses',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
