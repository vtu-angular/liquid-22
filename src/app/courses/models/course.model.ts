import { Category } from '../../categories/models/category.model';

export class Course {
  id!: number;
  created!: Date;
  lastUpdated!: Date;

  categoryId!: number;
  title!: string;
  subtitle!: string;
  language!: string;
  price!: number;
  discount!: number;
  isCertificateIncluded!: boolean;
  posterImgUrl!: string;
  publishAt!: Date;

  category!: Category;
}
