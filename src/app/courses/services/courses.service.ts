import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Course } from '../models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  constructor(private httpClient: HttpClient) {
  }

  getById$(id: number): Observable<Course> {
    const url = 'http://localhost:3000/courses/' + id;

    return this.httpClient.get<Course>(url);
  }

  getAll$(): Observable<Course[]> {
    const url = 'http://localhost:3000/courses';

    const httpParams = new HttpParams({
      fromObject: {
        _expand: 'category'
      }
    });

    return this.httpClient.get<Course[]>(url, {
      params: httpParams
    });
  }

  create$(course: Course): Observable<Course> {
    const url = 'http://localhost:3000/courses';

    course.created = new Date();
    course.lastUpdated = new Date();

    return this.httpClient.post<Course>(url, course);
  }

  update$(course: Course): Observable<Course> {
    const url = 'http://localhost:3000/courses/' + course.id;

    course.lastUpdated = new Date();

    return this.httpClient.patch<Course>(url, course);
  }

  save$(course: Course): Observable<Course> {
    if (course.id) {
      return this.update$(course);
    } else {
      return this.create$(course);
    }
  }

  delete$(id: number): Observable<void> {
    const url = 'http://localhost:3000/courses/' + id;

    return this.httpClient.delete<void>(url);
  }
}
