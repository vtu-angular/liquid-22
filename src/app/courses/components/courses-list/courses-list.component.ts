import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Course } from '../../models/course.model';
import { CoursesService } from '../../services/courses.service';
import { CourseDeleteDialogComponent } from '../course-delete-dialog/course-delete-dialog.component';

@Component({
  selector: 'app-courses-list',
  templateUrl: './courses-list.component.html',
  styleUrls: ['./courses-list.component.scss']
})
export class CoursesListComponent implements OnInit {

  courses!: Course[];

  constructor(private coursesService: CoursesService,
              private bsModalService: BsModalService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getAll();
  }

  onEditClick(course: Course): void {
    this.router.navigate(['courses', 'edit', course.id]);
  }

  onDeleteClick(course: Course): void {
    const ref = this.bsModalService.show(CourseDeleteDialogComponent, {
      initialState: {
        course: course
      }
    })

    if (ref.content) {
      ref.content.courseDeleted.subscribe({
        next: () => {
          this.getAll();
        }
      })
    }
  }

  private getAll(): void {
    this.coursesService.getAll$().subscribe({
      next: (response) => {
        this.courses = response;
      }
    })
  }

}
