import { Component, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { Course } from '../../models/course.model';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'app-course-delete-dialog',
  templateUrl: './course-delete-dialog.component.html',
  styleUrls: ['./course-delete-dialog.component.scss']
})
export class CourseDeleteDialogComponent {

  course!: Course;

  courseDeleted = new EventEmitter<void>();

  constructor(private coursesService: CoursesService,
              private bsModalRef: BsModalRef,
              private toastrService: ToastrService) {
  }

  deleteCourse(): void {
    this.coursesService.delete$(this.course.id).subscribe({
      next: () => {
        this.toastrService.success('Course was successfully deleted.', 'Success');
        this.hideDialog();
        this.courseDeleted.emit();
      }
    })
  }

  hideDialog(): void {
    this.bsModalRef.hide();
  }
}
