import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Category } from '../../../categories/models/category.model';
import { CategoriesService } from '../../../categories/services/categories.service';
import { Course } from '../../models/course.model';
import { CoursesService } from '../../services/courses.service';

@Component({
  selector: 'app-course-edit',
  templateUrl: './course-edit.component.html',
  styleUrls: ['./course-edit.component.scss']
})
export class CourseEditComponent implements OnInit {

  formGroup!: FormGroup;

  id!: number;
  course!: Course;

  categories!: Category[];

  constructor(private coursesService: CoursesService,
              private categoriesService: CategoriesService,
              private toastrService: ToastrService,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder) {
    const idParam = this.route.snapshot.paramMap.get('id');
    if (idParam) {
      this.id = +idParam;
    }
  }

  ngOnInit(): void {
    if (this.id) {
      this.coursesService.getById$(this.id).subscribe({
        next: (response) => {
          this.course = response;
          this.buildForm();
        }
      })
    } else {
      this.buildForm();
    }

    this.getCategories();
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      this.formGroup.markAllAsTouched();

      return;
    }

    const body: Course = {
      ...this.course,
      ...this.formGroup.value
    };

    this.coursesService.save$(body).subscribe({
      next: () => {
        this.toastrService.success('Course was successfully saved.', 'Success');
        this.router.navigate(['courses']);
      }
    });
  }

  private getCategories(): void {
    this.categoriesService.getAll$().subscribe({
      next: (response) => {
        this.categories = response;
      }
    })
  }

  private buildForm(): void {
    if (!this.course) {
      this.course = new Course();
    }

    this.formGroup = this.fb.group({
      title: [this.course.title, [Validators.required, Validators.minLength(3)]],
      subtitle: [this.course.subtitle, [Validators.required, Validators.minLength(3)]],
      categoryId: [this.course.categoryId, Validators.required],
      language: [this.course.language, Validators.required],
      price: [this.course.price, [Validators.required, Validators.min(0)]],
      discount: [this.course.discount, [Validators.min(0), Validators.max(100)]],
      posterImgUrl: this.course.posterImgUrl,
      isCertificateIncluded: this.course.isCertificateIncluded,
      publishAt: [this.course.publishAt, Validators.required]
    })
  }

}
